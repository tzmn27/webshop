<?php
session_start();

include("connection.php");
include("functions.php");

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Different</title>
        <!-- Favicon-->
        <link rel="manifest" href="/manifest.webmanifest">
        <link rel="icon" href="assets/favicon/favicon-32x32.ico" sizes="any">
        <link rel="apple-touch-icon" href="assets/favicon/apple-touch-icon-180x180.png">
        <!-- https://evilmartians.com/chronicles/how-to-favicon-in-2021-six-files-that-fit-most-needs -->
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">        
        <link href="css/styles.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Code example from: https://startbootstrap.com/template/shop-homepage -->
        <!-- Navigation-->
        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
            <div class="container px-4 px-lg-5">
                <a class="navbar-brand" href="#">Different</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                            <li class="nav-item"><a class="nav-link active" href="#">Home</a></li>
                            <li class="nav-item"><a class="nav-link" href="about.php">About</a></li>
                            <li class="nav-item"><a class="nav-link" href="shop.php">Shop</a></li>
                            </li>
                        </ul>
                        <?php

                        check_login($conn);

                        ?>
                </div>
            </div>
        </nav>
        <!-- Header-->
        <header class="bg-dark py-5">
            <div class="container px-4 px-lg-5 my-5">
                <div class="text-center text-white">
                    <h1 class="display-4 fw-bolder">Different</h1>
                    <p class="lead fw-normal text-white-50 mb-0">Shop simple, but different</p>
                </div>
            </div>
        </header>

        <!-- Slider -->

        <div class="container">
            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="false">
                <div class="carousel-indicators">
                  <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                  <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                  <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                </div>
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="assets/pictures/Kopfhörer.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>RAZER WEEK - Kopfhörer</h5>
                      <p>Hören sie jeden Ton, jeden Schritt, einfach alles</p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img src="assets/pictures/Maus.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>RAZER WEEK - Mäuse</h5>
                      <p>Genauigkeit eines Lasers, nie wieder daneben zielen</p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img src="assets/pictures/Tastatur.jpg" class="d-block w-100" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                      <h5>RAZER WEEK - Tastaturen</h5>
                      <p>Leise wie die Schritte eine Katze, schnell wie ein Puma</p>
                    </div>
                  </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>
              </div>
        </div>

        <div class="container" id="secHeader">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container-fluid">
                  <a class="navbar-brand noHover"href="#!">Kategorien</a>
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="nav navbar-nav">
                      <li class="nav-item">
                        <a class="nav-link active kategorie_text alle_texte" href="#!" onclick="reset_cards()">Alle</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link kategorie_text tastatur_texte" href="#!" onclick="hide_tastatur()">Tastaturen</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link kategorie_text maus_texte" href="#!" onclick="hide_maus()">Maus</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link kategorie_text monitor_texte" href="#!" onclick="hide_monitor()">Monitor</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link kategorie_text kopfhörer_texte" href="#!" onclick="hide_kopfhörer()">Kopfhörer</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
        </div>

        <div class="container" id="kategorien_index">
            <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                <div class="col mb-5 feature_card monitor">
                    <div class="card h-100">
                    <?php
                        $item = getKategorieItems($conn,1);
                        $id = $item['ID_Produkt'];
                        $name = $item['Name'];
                        $preis = $item['Preis'];
                        $bild = $item['Bild'];
                        $rabatt = $item['Rabatt'];
                        $stars = $item['Bewertung'];

                    ?>
                        <!-- Product image-->
                        <?php
                        if (empty($bild)){
                            echo "<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt=''...' />";    
                        }else{
                            echo "<img class='card-img-top' src=$bild alt=''...' />";
                        }
                        ?>
                        <!-- Product details-->
                        <div class="card-body p-4">
                            <div class="text-center">
                                <!-- Product name-->
                                <h5 class="fw-bolder">
                                    <?php
                                        echo "$name";
                                    ?>
                                </h5>
                                <?php
                                    $y = 0;
                                    echo "<div class='d-flex justify-content-center small text-warning mb-2'>";
                                    while ($y<$stars){
                                        echo "<div class='bi-star-fill'></div>";
                                        $y = $y + 1;
                                    }
                                    echo "</div>";
                                ?>
                                <!-- Product price-->
                                <?php
                                if (is_null($rabatt) || $rabatt == 0){
                                    echo "$preis €";
                                }else{
                                    $rabatt_preis = (100 - $rabatt)/100 * $preis;
                                    echo "<span class='text-muted text-decoration-line-through'>$preis €</span>";
                                    echo "  $rabatt_preis €"; 
                                }                                
                                ?>
                            </div>
                        </div>
                        <!-- Product actions-->
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                            <div class="text-center"><span><a class="btn btn-outline-dark mt-auto" href="product.php?pid=<?php echo $id; ?>">View options</a></span></div>
                        </div>
                    </div>
                </div>
                <div class="col mb-5 feature_card tastatur">
                    <div class="card h-100">
                        <?php
                            $item = getKategorieItems($conn,2);
                            $id = $item['ID_Produkt'];
                            $name = $item['Name'];
                            $preis = $item['Preis'];
                            $bild = $item['Bild'];
                            $rabatt = $item['Rabatt'];
                            $stars = $item['Bewertung'];

                        ?>
                                <!-- Product image-->
                        <?php
                            if (empty($bild)){
                                echo "<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt=''...' />";    
                            }else{
                                echo "<img class='card-img-top' src=$bild alt=''...' />";
                            }
                        ?>
                            <!-- Product details-->
                        <div class="card-body p-4">
                            <div class="text-center">
                                <!-- Product name-->
                                <h5 class="fw-bolder">
                                <?php
                                    echo "$name";
                                ?>
                                </h5>
                                <?php
                                    $y = 0;
                                    echo "<div class='d-flex justify-content-center small text-warning mb-2'>";
                                    while ($y<$stars){
                                        echo "<div class='bi-star-fill'></div>";
                                        $y = $y + 1;
                                    }
                                    echo "</div>";
                                ?>
                                <!-- Product price-->
                                <?php
                                if (is_null($rabatt) || $rabatt == 0){
                                    echo "$preis €";
                                }else{
                                    $rabatt_preis = (100 - $rabatt)/100 * $preis;
                                    echo "<span class='text-muted text-decoration-line-through'>$preis €</span>";
                                    echo "  $rabatt_preis €"; 
                                }                                
                                ?>
                                </div>
                        </div>
                        <!-- Product actions-->
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                            <div class="text-center"><span><a class="btn btn-outline-dark mt-auto" href="product.php?pid=<?php echo $id; ?>">View options</a></span></div>
                        </div>
                    </div>
                </div>
                        <div class="col mb-5 feature_card maus">
                            <div class="card h-100 ">
                            <?php
                                $item = getKategorieItems($conn,3);
                                $id = $item['ID_Produkt'];
                                $name = $item['Name'];
                                $preis = $item['Preis'];
                                $bild = $item['Bild'];
                                $rabatt = $item['Rabatt'];
                                $stars = $item['Bewertung'];
                            ?>
                            <!-- Product image-->
                            <?php
                            #assets/pictures/Maus_klein.jpg
                                if (empty($bild)){
                                        echo "<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt=''...' />";    
                                    }else{
                                        echo "<img class='card-img-top' src='$bild' alt=''...' />";
                                    }
                            ?>
                                <!-- Product details-->
                                <div class="card-body p-4">
                                    <div class="text-center">
                                        <!-- Product name-->
                                        <h5 class="fw-bolder">
                                    <?php
                                        echo "$name";
                                    ?>
                                    </h5>
                                    <?php
                                        $y = 0;
                                        echo "<div class='d-flex justify-content-center small text-warning mb-2'>";
                                        while ($y<$stars){
                                            echo "<div class='bi-star-fill'></div>";
                                            $y = $y + 1;
                                        }
                                        echo "</div>";
                                    ?>
                                    <!-- Product price-->
                                    <?php
                                if (is_null($rabatt) || $rabatt == 0){
                                    echo "$preis €";
                                }else{
                                    $rabatt_preis = (100 - $rabatt)/100 * $preis;
                                    echo "<span class='text-muted text-decoration-line-through'>$preis €</span>";
                                    echo "  $rabatt_preis €"; 
                                }                                
                                ?>
                                    </div>
                                </div>
                                <!-- Product actions-->
                                <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div class="text-center"><span><a class="btn btn-outline-dark mt-auto" href="product.php?pid=<?php echo $id; ?>">View options</a></span></div>
                                </div>
                            </div>
                        </div>
                            <div class="col mb-5 feature_card kopfhörer">
                                <div class="card h-100">
                                <?php
                                    $item = getKategorieItems($conn,4);
                                    $id = $item['ID_Produkt'];
                                    $name = $item['Name'];
                                    $preis = $item['Preis'];
                                    $bild = $item['Bild'];
                                    $rabatt = $item['Rabatt'];
                                    $stars = $item['Bewertung'];

                                ?>
                                <!-- Product image-->
                                <?php
                                    if (empty($bild)){
                                        echo "<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt=''...' />";    
                                    }else{
                                        echo "<img class='card-img-top' src=$bild alt=''...' />";
                                    }
                                ?>
                                <!-- Product details-->
                                    <div class="card-body p-4">
                                        <div class="text-center">
                                            <!-- Product name-->
                                            <h5 class="fw-bolder">                                    
                                    <?php
                                        echo "$name";
                                    ?>
                                    </h5>
                                    <?php
                                        $y = 0;
                                        echo "<div class='d-flex justify-content-center small text-warning mb-2'>";
                                        while ($y<$stars){
                                            echo "<div class='bi-star-fill'></div>";
                                            $y = $y + 1;
                                        }
                                        echo "</div>";
                                    ?>
                                    <!-- Product price-->
                                    <?php
                                    if (is_null($rabatt) || $rabatt == 0){
                                        echo "$preis €";
                                    }else{
                                        $rabatt_preis = (100 - $rabatt)/100 * $preis;
                                        echo "<span class='text-muted text-decoration-line-through'>$preis €</span>";
                                        echo "  $rabatt_preis €"; 
                                    }                                
                                    ?>
                                        </div>
                                    </div>
                                    <!-- Product actions-->
                                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                        <div class="text-center"><span><a class="btn btn-outline-dark mt-auto" href="product.php?pid=<?php echo $id; ?>">View options</a></span></div>
                                    </div>
                                </div>
                            </div>    
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-no-padding">
                  <img class="thumbnail img-fluid" src="assets/pictures/banner-1.jpg">
                </div>
                <div class="col-xs-12 col-sm-6 col-no-padding">
                  <img class="thumbnail img-fluid" src="assets/pictures/banner-2.jpg">
                </div>
            </div>
        </div>

        <!-- Section-->
        <section class="py-5">
            <div class="container px-4 px-lg-5 mt-5">
                <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                    <div class="col mb-5">
                        <div class="card h-100">
                        <?php
                            $item = getKategorieItems($conn,4);
                            $id = $item['ID_Produkt'];
                            $name = $item['Name'];
                            $preis = $item['Preis'];
                            $bild = $item['Bild'];
                            $rabatt = $item['Rabatt'];
                            $stars = $item['Bewertung'];

                        ?>
                            <!-- Product image-->
                            <?php
                            if (empty($bild)){
                                echo "<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt=''...' />";    
                            }else{
                                echo "<img class='card-img-top' src=$bild alt=''...' />";
                            }
                            ?>
                            <!-- Product details-->
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <!-- Product name-->
                                    <h5 class="fw-bolder">
                                        <?php
                                            echo "$name";
                                        ?>
                                    </h5>
                                    <?php
                                        $y = 0;
                                        echo "<div class='d-flex justify-content-center small text-warning mb-2'>";
                                        while ($y<$stars){
                                            echo "<div class='bi-star-fill'></div>";
                                            $y = $y + 1;
                                        }
                                        echo "</div>";
                                    ?>
                                    <!-- Product price-->
                                    <?php
                                    if (is_null($rabatt) || $rabatt == 0){
                                        echo "$preis €";
                                    }else{
                                        $rabatt_preis = (100 - $rabatt)/100 * $preis;
                                        echo "<span class='text-muted text-decoration-line-through'>$preis €</span>";
                                        echo "  $rabatt_preis €"; 
                                    }                                
                                    ?>
                                </div>
                            </div>
                        <!-- Product actions-->
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                            <div class="text-center"><span><a class="btn btn-outline-dark mt-auto" href="product.php?pid=<?php echo $id; ?>">View options</a></span></div>
                        </div>
                    </div>
                </div>
                    <div class="col mb-5">
                        <div class="card h-100">
                            <!-- Sale badge-->
                            <div class="badge bg-dark text-white position-absolute" style="top: 0.5rem; right: 0.5rem">Sale</div>
                            <?php
                            $item = getKategorieItems($conn,1);
                            $id = $item['ID_Produkt'];
                            $name = $item['Name'];
                            $preis = $item['Preis'];
                            $bild = $item['Bild'];
                            $rabatt = $item['Rabatt'];
                            $stars = $item['Bewertung'];

                        ?>
                            <!-- Product image-->
                            <?php
                            if (empty($bild)){
                                echo "<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt=''...' />";    
                            }else{
                                echo "<img class='card-img-top' src=$bild alt=''...' />";
                            }
                            ?>
                            <!-- Product details-->
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <!-- Product name-->
                                    <h5 class="fw-bolder">
                                        <?php
                                            echo "$name";
                                        ?>
                                    </h5>
                                    <?php
                                        $y = 0;
                                        echo "<div class='d-flex justify-content-center small text-warning mb-2'>";
                                        while ($y<$stars){
                                            echo "<div class='bi-star-fill'></div>";
                                            $y = $y + 1;
                                        }
                                        echo "</div>";
                                    ?>
                                    <!-- Product price-->
                                    <?php
                                    if (is_null($rabatt) || $rabatt == 0){
                                        echo "$preis €";
                                    }else{
                                        $rabatt_preis = (100 - $rabatt)/100 * $preis;
                                        echo "<span class='text-muted text-decoration-line-through'>$preis €</span>";
                                        echo "  $rabatt_preis €"; 
                                    }                                
                                    ?>
                                </div>
                            </div>
                        <!-- Product actions-->
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                            <div class="text-center"><span><a class="btn btn-outline-dark mt-auto" href="product.php?pid=<?php echo $id; ?>">View options</a></span></div>
                        </div>
                    </div>
                </div>
                    <div class="col mb-5">
                        <div class="card h-100">
                            <!-- Sale badge-->
                            <div class="badge bg-dark text-white position-absolute" style="top: 0.5rem; right: 0.5rem">Sale</div>
                            <?php
                            $item = getKategorieItemsSale($conn,2);
                            $name = $item['Name'];
                            $id = $item['ID_Produkt'];
                            $preis = $item['Preis'];
                            $bild = $item['Bild'];
                            $rabatt = $item['Rabatt'];
                            $stars = $item['Bewertung'];

                        ?>
                                <!-- Product image-->
                        <?php
                            if (empty($bild)){
                                echo "<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt=''...' />";    
                            }else{
                                echo "<img class='card-img-top' src=$bild alt=''...' />";
                            }
                        ?>
                            <!-- Product details-->
                        <div class="card-body p-4">
                            <div class="text-center">
                                <!-- Product name-->
                                <h5 class="fw-bolder">
                                <?php
                                    echo "$name";
                                ?>
                                </h5>

                                <?php
                                    $y = 0;
                                    echo "<div class='d-flex justify-content-center small text-warning mb-2'>";
                                    while ($y<$stars){
                                        echo "<div class='bi-star-fill'></div>";
                                        $y = $y + 1;
                                    }
                                    echo "</div>";
                                ?>
                                <!-- Product price-->
                                <?php
                                if (is_null($rabatt) || $rabatt == 0){
                                    echo "$preis €";
                                }else{
                                    $rabatt_preis = (100 - $rabatt)/100 * $preis;
                                    echo "<span class='text-muted text-decoration-line-through'>$preis €</span>";
                                    echo "  $rabatt_preis €"; 
                                }                                
                                ?>
                                </div>
                        </div>
                        <!-- Product actions-->
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                            <div class="text-center"><span><a class="btn btn-outline-dark mt-auto" href="product.php?pid=<?php echo $id; ?>">View options</a></span></div>
                        </div>
                    </div>
                </div>
                    <div class="col mb-5">
                        <div class="card h-100">
                        <?php
                            $item = getKategorieItemsSale($conn,3);
                            $id = $item['ID_Produkt'];
                            $name = $item['Name'];
                            $preis = $item['Preis'];
                            $bild = $item['Bild'];
                            $rabatt = $item['Rabatt'];
                            $stars = $item['Bewertung'];

                        ?>
                                <!-- Product image-->
                        <?php
                            if (empty($bild)){
                                echo "<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt=''...' />";    
                            }else{
                                echo "<img class='card-img-top' src=$bild alt=''...' />";
                            }
                        ?>
                            <!-- Product details-->
                        <div class="card-body p-4">
                            <div class="text-center">
                                <!-- Product name-->
                                <h5 class="fw-bolder">
                                <?php
                                    echo "$name";
                                ?>
                                </h5>

                                <?php
                                    $y = 0;
                                    echo "<div class='d-flex justify-content-center small text-warning mb-2'>";
                                    while ($y<$stars){
                                        echo "<div class='bi-star-fill'></div>";
                                        $y = $y + 1;
                                    }
                                    echo "</div>";
                                ?>
                                <!-- Product price-->
                                <?php
                                if (is_null($rabatt) || $rabatt == 0){
                                    echo "$preis €";
                                }else{
                                    $rabatt_preis = (100 - $rabatt)/100 * $preis;
                                    echo "<span class='text-muted text-decoration-line-through'>$preis €</span>";
                                    echo "  $rabatt_preis €"; 
                                }                                
                                ?>
                                </div>
                        </div>
                        <!-- Product actions-->
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                            <div class="text-center"><span><a class="btn btn-outline-dark mt-auto" href="product.php?pid=<?php echo $id; ?>">View options</a></span></div>
                        </div>
                    </div>
                </div>      
            </div>
        </section>
        <!-- Footer-->
        <footer class="py-5 bg-dark text-center">
                <div class="container">
                  <ul class="list-inline">
                    <li class="list-inline-item">
                        <!-- Modal -->
                        <a href="#modalDatenschutz" data-bs-toggle="modal">Datenschutz</a>
                        <div class="modal fade" id="modalDatenschutz" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Datenschutz</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                              </div>
                              <div class="modal-body">
                                <h1>Datenschutz&shy;erkl&auml;rung</h1>
                                <h2>1. Datenschutz auf einen Blick</h2>
                                <h3>Allgemeine Hinweise</h3> <p>Die folgenden Hinweise geben einen einfachen &Uuml;berblick dar&uuml;ber, was mit Ihren personenbezogenen Daten passiert, wenn Sie diese Website besuchen. Personenbezogene Daten sind alle Daten, mit denen Sie pers&ouml;nlich identifiziert werden k&ouml;nnen. Ausf&uuml;hrliche Informationen zum Thema Datenschutz entnehmen Sie unserer unter diesem Text aufgef&uuml;hrten Datenschutzerkl&auml;rung.</p>
                                <h3>Datenerfassung auf dieser Website</h3> <h4>Wer ist verantwortlich f&uuml;r die Datenerfassung auf dieser Website?</h4> <p>Die Datenverarbeitung auf dieser Website erfolgt durch den Websitebetreiber. Dessen Kontaktdaten k&ouml;nnen Sie dem Impressum dieser Website entnehmen.</p> <h4>Wie erfassen wir Ihre Daten?</h4> <p>Ihre Daten werden zum einen dadurch erhoben, dass Sie uns diese mitteilen. Hierbei kann es sich z.&nbsp;B. um Daten handeln, die Sie in ein Kontaktformular eingeben.</p> <p>Andere Daten werden automatisch oder nach Ihrer Einwilligung beim Besuch der Website durch unsere IT-Systeme erfasst. Das sind vor allem technische Daten (z.&nbsp;B. Internetbrowser, Betriebssystem oder Uhrzeit des Seitenaufrufs). Die Erfassung dieser Daten erfolgt automatisch, sobald Sie diese Website betreten.</p> <h4>Wof&uuml;r nutzen wir Ihre Daten?</h4> <p>Ein Teil der Daten wird erhoben, um eine fehlerfreie Bereitstellung der Website zu gew&auml;hrleisten. Andere Daten k&ouml;nnen zur Analyse Ihres Nutzerverhaltens verwendet werden.</p> <h4>Welche Rechte haben Sie bez&uuml;glich Ihrer Daten?</h4> <p>Sie haben jederzeit das Recht, unentgeltlich Auskunft &uuml;ber Herkunft, Empf&auml;nger und Zweck Ihrer gespeicherten personenbezogenen Daten zu erhalten. Sie haben au&szlig;erdem ein Recht, die Berichtigung oder L&ouml;schung dieser Daten zu verlangen. Wenn Sie eine Einwilligung zur Datenverarbeitung erteilt haben, k&ouml;nnen Sie diese Einwilligung jederzeit f&uuml;r die Zukunft widerrufen. Au&szlig;erdem haben Sie das Recht, unter bestimmten Umst&auml;nden die Einschr&auml;nkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Des Weiteren steht Ihnen ein Beschwerderecht bei der zust&auml;ndigen Aufsichtsbeh&ouml;rde zu.</p> <p>Hierzu sowie zu weiteren Fragen zum Thema Datenschutz k&ouml;nnen Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden.</p>
                                <h2>2. Hosting und Content Delivery Networks (CDN)</h2>
                                <h3>Externes Hosting</h3> <p>Diese Website wird bei einem externen Dienstleister gehostet (Hoster). Die personenbezogenen Daten, die auf dieser Website erfasst werden, werden auf den Servern des Hosters gespeichert. Hierbei kann es sich v. a. um IP-Adressen, Kontaktanfragen, Meta- und Kommunikationsdaten, Vertragsdaten, Kontaktdaten, Namen, Websitezugriffe und sonstige Daten, die &uuml;ber eine Website generiert werden, handeln.</p> <p>Der Einsatz des Hosters erfolgt zum Zwecke der Vertragserf&uuml;llung gegen&uuml;ber unseren potenziellen und bestehenden Kunden (Art. 6 Abs. 1 lit. b DSGVO) und im Interesse einer sicheren, schnellen und effizienten Bereitstellung unseres Online-Angebots durch einen professionellen Anbieter (Art. 6 Abs. 1 lit. f DSGVO).</p> <p>Unser Hoster wird Ihre Daten nur insoweit verarbeiten, wie dies zur Erf&uuml;llung seiner Leistungspflichten erforderlich ist und unsere Weisungen in Bezug auf diese Daten befolgen.</p> <p>Wir setzen folgenden Hoster ein:</p>
                                <p>
                                  Technische Hochschule Mittelhessen<br />
                                  University of Applied Sciences<br />
                                  Fachbereich 11 - IEM<br />
                                  Informationstechnik - Elektrotechnik - Mechatronik<br />
                                  <br />
                                  Anschrift: <br />
                                  Wilhelm-Leuschner-Straße 13<br />
                                  D - 61169 Friedberg
                                </p>
                                <h2>3. Allgemeine Hinweise und Pflicht&shy;informationen</h2>
                                <h3>Datenschutz</h3> <p>Die Betreiber dieser Seiten nehmen den Schutz Ihrer pers&ouml;nlichen Daten sehr ernst. Wir behandeln Ihre personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerkl&auml;rung.</p> <p>Wenn Sie diese Website benutzen, werden verschiedene personenbezogene Daten erhoben. Personenbezogene Daten sind Daten, mit denen Sie pers&ouml;nlich identifiziert werden k&ouml;nnen. Die vorliegende Datenschutzerkl&auml;rung erl&auml;utert, welche Daten wir erheben und wof&uuml;r wir sie nutzen. Sie erl&auml;utert auch, wie und zu welchem Zweck das geschieht.</p> <p>Wir weisen darauf hin, dass die Daten&uuml;bertragung im Internet (z.&nbsp;B. bei der Kommunikation per E-Mail) Sicherheitsl&uuml;cken aufweisen kann. Ein l&uuml;ckenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht m&ouml;glich.</p>
                                <h3>Hinweis zur verantwortlichen Stelle</h3> <p>Die verantwortliche Stelle f&uuml;r die Datenverarbeitung auf dieser Website ist:</p> <p>Thomas Zimmermann<br />
                                Hermann-L&ouml;ns Weg 13a<br />
                                35447 Reiskirchen</p>
              
                                <p>
                                  <a href="mailto:thomas.zimmermann@mni.thm.de">thomas.zimmermann@mnd.thm.de</a>
                                </p>
                                <p>Verantwortliche Stelle ist die nat&uuml;rliche oder juristische Person, die allein oder gemeinsam mit anderen &uuml;ber die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten (z.&nbsp;B. Namen, E-Mail-Adressen o. &Auml;.) entscheidet.</p>
              
                                <h3>Speicherdauer</h3>
                                <p>Soweit innerhalb dieser Datenschutzerkl&auml;rung keine speziellere Speicherdauer genannt wurde, verbleiben Ihre personenbezogenen Daten bei uns, bis der Zweck f&uuml;r die Datenverarbeitung entf&auml;llt. Wenn Sie ein berechtigtes L&ouml;schersuchen geltend machen oder eine Einwilligung zur Datenverarbeitung widerrufen, werden Ihre Daten gel&ouml;scht, sofern wir keinen anderen rechtlich zul&auml;ssigen&nbsp; Gr&uuml;nde f&uuml;r die Speicherung Ihrer personenbezogenen Daten haben (z.B. steuer- oder handelsrechtliche Aufbewahrungsfristen); im letztgenannten Fall erfolgt die L&ouml;schung nach Fortfall dieser Gr&uuml;nde.</p>
                                <h3>Widerruf Ihrer Einwilligung zur Datenverarbeitung</h3>
                                <p>Viele Datenverarbeitungsvorg&auml;nge sind nur mit Ihrer ausdr&uuml;cklichen Einwilligung m&ouml;glich. Sie k&ouml;nnen eine bereits erteilte Einwilligung jederzeit widerrufen. Die Rechtm&auml;&szlig;igkeit der bis zum Widerruf erfolgten Datenverarbeitung bleibt vom Widerruf unber&uuml;hrt.</p>
                                <h3>Widerspruchsrecht gegen die Datenerhebung in besonderen F&auml;llen sowie gegen Direktwerbung (Art. 21 DSGVO)</h3>
                                  <p>Wenn die Datenverarbeitung auf Grundlage von Art. 6 Abs. 1 lit. e oder f DSGVO erfolgt, haben Sie jederzeit das Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, gegen die Verarbeitung Ihrer personenbezogenen Daten Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmungen gestütztes Profiling. Die jeweilige Rechtsgrundlage, auf denen eine Verarbeitung beruht, entnehmen Sie dieser Datenschutzerklärung. Wenn Sie Widerspruch einlegen, werden wir Ihre betroffenen personenbezogenen Daten nicht mehr verarbeiten, es sei denn, wir können zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen (Widerspruch nach Art. 21 abs. 1 DSGVO).</p>
                                  <p>Werden Ihre personenbezogenen Daten verarbeitet, um Direktwerbung zu betreiben, so haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung Sie betreffender personenbezogener Daten zum Zwecke derartiger Werbung einzulegen; dies gilt auch für das Profiling, soweit es mit solcher Direktwerbung in Verbindung steht. Wenn Sie widersprechen, werden Ihre personenbezogenen Daten anschließend nicht mehr zum Zwecke der Direktwerbung verwendet (Widerspruch nach Art. 21 Abs. 2 DSGVO).</p>
                                <h3>Beschwerde&shy;recht bei der zust&auml;ndigen Aufsichts&shy;beh&ouml;rde</h3>
                                <p>Im Falle von Verst&ouml;&szlig;en gegen die DSGVO steht den Betroffenen ein Beschwerderecht bei einer Aufsichtsbeh&ouml;rde, insbesondere in dem Mitgliedstaat ihres gew&ouml;hnlichen Aufenthalts, ihres Arbeitsplatzes oder des Orts des mutma&szlig;lichen Versto&szlig;es zu. Das Beschwerderecht besteht unbeschadet anderweitiger verwaltungsrechtlicher oder gerichtlicher Rechtsbehelfe.</p>
                                <h3>Recht auf Daten&shy;&uuml;bertrag&shy;barkeit</h3>
                                <p>Sie haben das Recht, Daten, die wir auf Grundlage Ihrer Einwilligung oder in Erf&uuml;llung eines Vertrags automatisiert verarbeiten, an sich oder an einen Dritten in einem g&auml;ngigen, maschinenlesbaren Format aush&auml;ndigen zu lassen. Sofern Sie die direkte &Uuml;bertragung der Daten an einen anderen Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch machbar ist.</p>
                                <h3>Auskunft, L&ouml;schung und Berichtigung</h3>
                                <p>Sie haben im Rahmen der geltenden gesetzlichen Bestimmungen jederzeit das Recht auf unentgeltliche Auskunft &uuml;ber Ihre gespeicherten personenbezogenen Daten, deren Herkunft und Empf&auml;nger und den Zweck der Datenverarbeitung und ggf. ein Recht auf Berichtigung oder L&ouml;schung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene Daten k&ouml;nnen Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden.</p>
                                <h3>Recht auf Einschr&auml;nkung der Verarbeitung</h3>
                                <p>Sie haben das Recht, die Einschr&auml;nkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Hierzu k&ouml;nnen Sie sich jederzeit unter der im Impressum angegebenen Adresse an uns wenden. Das Recht auf Einschr&auml;nkung der Verarbeitung besteht in folgenden F&auml;llen:</p>
                                <ul>
                                  <li>Wenn Sie die Richtigkeit Ihrer bei uns gespeicherten personenbezogenen Daten bestreiten, ben&ouml;tigen wir in der Regel Zeit, um dies zu &uuml;berpr&uuml;fen. F&uuml;r die Dauer der Pr&uuml;fung haben Sie das Recht, die Einschr&auml;nkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen.</li>
                                  <li>Wenn die Verarbeitung Ihrer personenbezogenen Daten unrechtm&auml;&szlig;ig geschah/geschieht, k&ouml;nnen Sie statt der L&ouml;schung die Einschr&auml;nkung der Datenverarbeitung verlangen.</li>
                                  <li>Wenn wir Ihre personenbezogenen Daten nicht mehr ben&ouml;tigen, Sie sie jedoch zur Aus&uuml;bung, Verteidigung oder Geltendmachung von Rechtsanspr&uuml;chen ben&ouml;tigen, haben Sie das Recht, statt der L&ouml;schung die Einschr&auml;nkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen.</li>
                                  <li>Wenn Sie einen Widerspruch nach Art. 21 Abs. 1 DSGVO eingelegt haben, muss eine Abw&auml;gung zwischen Ihren und unseren Interessen vorgenommen werden. Solange noch nicht feststeht, wessen Interessen &uuml;berwiegen, haben Sie das Recht, die Einschr&auml;nkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen.</li>
                                </ul>
                                <p>Wenn Sie die Verarbeitung Ihrer personenbezogenen Daten eingeschr&auml;nkt haben, d&uuml;rfen diese Daten &ndash; von ihrer Speicherung abgesehen &ndash; nur mit Ihrer Einwilligung oder zur Geltendmachung, Aus&uuml;bung oder Verteidigung von Rechtsanspr&uuml;chen oder zum Schutz der Rechte einer anderen nat&uuml;rlichen oder juristischen Person oder aus Gr&uuml;nden eines wichtigen &ouml;ffentlichen Interesses der Europ&auml;ischen Union oder eines Mitgliedstaats verarbeitet werden.</p>
                                <h2>4. Datenerfassung auf dieser Website</h2>
                                <h3>Cookies</h3>
                                <p>Unsere Internetseiten verwenden so genannte &bdquo;Cookies&ldquo;. Cookies sind kleine Textdateien und richten auf Ihrem Endger&auml;t keinen Schaden an. Sie werden entweder vor&uuml;bergehend f&uuml;r die Dauer einer Sitzung (Session-Cookies) oder dauerhaft (permanente Cookies) auf Ihrem Endger&auml;t gespeichert. Session-Cookies werden nach Ende Ihres Besuchs automatisch gel&ouml;scht. Permanente Cookies bleiben auf Ihrem Endger&auml;t gespeichert, bis Sie diese selbst l&ouml;schen&nbsp;oder eine automatische L&ouml;schung durch Ihren Webbrowser erfolgt.</p>
                                <p>Teilweise k&ouml;nnen auch Cookies von Drittunternehmen auf Ihrem Endger&auml;t gespeichert werden, wenn Sie unsere Seite betreten (Third-Party-Cookies). Diese erm&ouml;glichen uns oder Ihnen die Nutzung bestimmter Dienstleistungen des Drittunternehmens (z.B. Cookies zur Abwicklung von Zahlungsdienstleistungen).</p>
                                <p>Cookies haben verschiedene Funktionen. Zahlreiche Cookies sind technisch notwendig, da bestimmte Websitefunktionen ohne diese nicht funktionieren w&uuml;rden (z.B. die Warenkorbfunktion oder die Anzeige von Videos). Andere Cookies dienen dazu, das Nutzerverhalten auszuwerten&nbsp;oder Werbung anzuzeigen.</p> <p>Cookies, die zur Durchf&uuml;hrung des elektronischen Kommunikationsvorgangs (notwendige Cookies) oder zur Bereitstellung bestimmter, von Ihnen erw&uuml;nschter Funktionen (funktionale Cookies, z. B. f&uuml;r die Warenkorbfunktion) oder zur Optimierung der Website (z.B. Cookies zur Messung des Webpublikums) erforderlich sind, werden auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO gespeichert, sofern keine andere Rechtsgrundlage angegeben wird. Der Websitebetreiber hat ein berechtigtes Interesse an der Speicherung von Cookies zur technisch fehlerfreien und optimierten Bereitstellung seiner Dienste. Sofern eine Einwilligung zur Speicherung von Cookies abgefragt wurde, erfolgt die Speicherung der betreffenden Cookies ausschlie&szlig;lich auf Grundlage dieser Einwilligung (Art. 6 Abs. 1 lit. a DSGVO); die Einwilligung ist jederzeit widerrufbar.</p>
                                <p>Sie k&ouml;nnen Ihren Browser so einstellen, dass Sie &uuml;ber das Setzen von Cookies informiert werden und Cookies nur im Einzelfall erlauben, die Annahme von Cookies f&uuml;r bestimmte F&auml;lle oder generell ausschlie&szlig;en sowie das automatische L&ouml;schen der Cookies beim Schlie&szlig;en des Browsers aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalit&auml;t dieser Website eingeschr&auml;nkt sein.</p> <p>Soweit Cookies von Drittunternehmen oder zu Analysezwecken eingesetzt werden, werden wir Sie hier&uuml;ber im Rahmen dieser Datenschutzerkl&auml;rung gesondert informieren und ggf. eine Einwilligung abfragen.</p>
                                <p>Quelle:
                                  <a href="https://www.e-recht24.de">eRecht24</a>
                                </p>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </li>
                    <li class="list-inline-item">
                      <!-- Modal -->
                      <a href="#modalImpressum" data-bs-toggle="modal">Impressum</a>
                      <div class="modal fade" id="modalImpressum" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                              <h1>Impressum</h1>
                              <h3>Feedback</h3>
                              <p>
                                Wir freuen uns über Ihr Feedback zur Barrierefreiheit von Different. Bitte lassen Sie es uns wissen, wenn Sie auf Barrieren stoßen:
                                <br>Mail: <a href="mailto:thomas.zimmermann@mni.thm.de">thomas.zimmermann@mnd.thm.de</a>
                              </p>          
                              <h2>Angaben gem&auml;&szlig; &sect; 5 TMG</h2>
                              <p>Max Mustermann<br />
                              Wilhelm-Leuschner-Stra&szlig;e 13<br />
                              61169 Friedberg</p>
                              
                              <h2>Kontakt</h2>
                              <p>Telefon: &#91;Telefonnummer&#93;<br />
                              E-Mail: <a href="mailto:thomas.zimmermann@mni.thm.de">thomas.zimmermann@mnd.thm.de</a></p>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>                       
                    </li>
                    <li id="footer_copyright">©Different</li>
                  </ul>
                </div>      
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- Cookie Consent -->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
        <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
        <script>
            window.cookieconsent.initialise({
            "palette": {
                "popup": {
                "background": "#e07211"
                },
                "button": {
                "background": "#003326"
                }
            },
            "color": "black",
            "theme": "classic",
            "position": "bottom-right",
            "type": "opt-in",
            "content": {
                "message": "Diese Website verwendet Cookies. Mit Anklicken des Buttons »Akzeptieren« erklärst du dich mit der Verwendung der Cookies und unserer Daten­schutz­erklärung einverstanden. Was Cookies sind kannst du unter »Mehr erfahren« einsehen.",
                "dismiss": "Ablehnen",
                "allow": "Akzeptieren",
                "link": "Mehr erfahren"
            }
            });
        </script>
    </body>
</html>
