/*!
 * Start Bootstrap - Shop Homepage v5.0.5 (https://startbootstrap.com/template/shop-homepage)
 * Copyright 2013-2022 Start Bootstrap
 * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-shop-homepage/blob/master/LICENSE)
 */
// This file is intentionally blank
// Use this file to add JavaScript to your project

const cards = document.querySelectorAll(".feature_card");
const monitore = document.querySelectorAll(".monitor");
const tastaturen = document.querySelectorAll(".tastatur");
const mäuse = document.querySelectorAll(".maus");
const kopfhörer = document.querySelectorAll(".kopfhörer");

const texte = document.querySelectorAll(".kategorie_text")
const monitor_text = document.querySelectorAll(".monitor_texte");
const tastatur_text = document.querySelectorAll(".tastatur_texte");
const maus_text = document.querySelectorAll(".maus_texte");
const kopfhörer_text = document.querySelectorAll(".kopfhörer_texte");
const alle_text = document.querySelectorAll(".alle_texte");

function remove() {
  for (const text of texte) {
    text.classList.remove("active");
  }
  for (const card of cards) {
    card.classList.add("d-none");
  }
}

function hide_monitor() {

  remove();

  for (const monitor of monitore) {
    monitor.classList.remove("d-none");
  }

  for (const montext of monitor_text) {
    montext.classList.add("active");
  }

}

function hide_tastatur() {

  remove();

  for (const tastatur of tastaturen) {
    tastatur.classList.remove("d-none");
  }

  for (const tasttext of tastatur_text) {
    tasttext.classList.add("active");
  }

}

function hide_maus() {

  remove();

  for (const maus of mäuse) {
    maus.classList.remove("d-none");
  }
  
  for (const maustext of maus_text) {
    maustext.classList.add("active");
  }

}

function hide_kopfhörer() {

  remove();

  for (const kopfhörer_ of kopfhörer) {
    kopfhörer_.classList.remove("d-none");
  }
  
  for (const kopftext of kopfhörer_text) {
    kopftext.classList.add("active");
  }

}

function reset_cards() {
  for (const text of texte) {
    text.classList.remove("active");
  }
  for (const card of cards) {
    card.classList.remove("d-none");
  }
  for (const alltext of alle_text) {
    alltext.classList.add("active");
  }
}

// fix footer on mobile screens
function foot(){
  if (screen.width > 760) {
    var element = document.getElementById("footer_cs");
    element.classList.add("fixed-bottom");
  }
}

function myfunction(){
  alert("Keine Zahlungsdaten!");
}