Different
=========

Projekt I

Prof. Dr. Carsten Lucke

Die Website können Sie <a href="https://hosting.iem.thm.de/user/dhnn28/Webshop_Neu/webshop/index.php">hier</a> erreichen (CAS-Login nötig).

Einführung
---------

Mit Different haben wir einen Onlineshop erstellt, der einfach ist, aber doch anders als andere.
Wir haben besonders werden auf responsives Design gelegt, ebenso sollte der Shop so einfach wie möglich gestaltet sein, um das shoppen simpel zu halten.
Die Artikel werden dynamisch aus einer Datenbank abgerufen, es gibt einen Warenkorb sowie ein funktionierendes Login-System.
Das Profil kann man auch im nachhinein noch verwalten und auch ändern.


Installation
---------
1. Sie müssen eine Datenbank haben, über die Sie Artikel und Profildaten speichern können.

2. Das Projekt benutzt keine extra Frameworks, also klonen Sie sich einfach das Projekt und legen Sie los!

Datenbank
---------

1. Erstellen der Tabellen
---------
Kunde:
---------
CREATE TABLE Benutzer (ID_Nutzer int(5) PRIMARY KEY AUTO_INCREMENT, Vorname varchar (50), Nachname varchar (50), Strasse varchar (50), Ort varchar (50), Mail varchar (100), Passwort varchar (60), Zahlungsdaten varchar (100));

Kategorie:
---------
CREATE TABLE Kategorie (ID_Kategorie int(5) PRIMARY KEY AUTO_INCREMENT, Name varchar (100), Eigenschaft varchar (250));

Produkttabelle:
---------
CREATE TABLE Produkttabelle (ID_Produkt int(10) PRIMARY KEY, ID_Kategorie int, Name varchar (100), Beschreibung varchar (400), Preis float (10), Tags varchar (250), Infos varchar (250), Bewertung int (1), Rabatt int (5), Bild varchar (150), FOREIGN KEY(ID_Kategorie) REFERENCES Kategorie (ID_Kategorie));
Warenkorb:

CREATE TABLE Warenkorb (ID_Warenkorb int(10) PRIMARY KEY AUTO_INCREMENT, ID_Nutzer int, ID_Produkt int, Anzahl int (10), Gesamt float(10), FOREIGN KEY(ID_Nutzer) REFERENCES Benutzer (ID_Nutzer), FOREIGN KEY(ID_Produkt) REFERENCES Produkttabelle (ID_Produkt));

Bestellung:
---------
CREATE TABLE Bestellungen (ID_Bestellung int(10), ID_Nutzer int, ID_Produkt int, Anzahl int (10), Gesamt float(10));

Schlüssel setzten für Bestellung: 
---------
CREATE UNIQUE INDEX id_b on Bestellungen (ID_Bestellung, ID_Nutzer, ID_Produkt);

Rechnung:
---------
CREATE TABLE Rechnung (ID_Rechnung int(10) PRIMARY KEY AUTO_INCREMENT, ID_Nutzer int, Gesamt float(10), FOREIGN KEY(ID_Nutzer) REFERENCES Benutzer (ID_Nutzer));


2. Füllen der Tabellen:
---------
Kategorie:
---------
INSERT INTO `Kategorie`(`ID_Kategorie`, `Name`, `Eigenschaft`) VALUES (1, 'Monitor','Hertz, Helligkeit, PPI'),(2,'Tastatur','Mechanisch'),(3,'Maus','Hz'),(4,'Kopfhoerer','Lautstärke');

Produkte:
---------
INSERT INTO `Produkttabelle`(`ID_Produkt`, `ID_Kategorie`, `Name`, `Beschreibung`, `Preis`, `Tags`, `Infos`, `Bewertung`, `Rabatt`, `Bild`) VALUES (1,1,'Hyper Y Monitor','Ein Bildschirm (auch Monitor bzw. englisch Screen oder Display) ist eine elektrisch angesteuerte Anzeige ohne bewegliche Teile zur optischen Signalisierung von veränderlichen Informationen wie Bildern oder Zeichen.',100,'Bild, TV','',3,5,'assets/pictures/Monitor1.jpg'),
(2,1,'Hyper A Monitor','Ein Bildschirm (auch Monitor bzw. englisch Screen oder Display) ist eine elektrisch angesteuerte Anzeige ohne bewegliche Teile zur optischen Signalisierung von veränderlichen Informationen wie Bildern oder Zeichen.',125,'Bild, TV','',4, '','assets/pictures/Monitor2.jpg'),
(3,1,'Hyper B Monitor','Ein Bildschirm (auch Monitor bzw. englisch Screen oder Display) ist eine elektrisch angesteuerte Anzeige ohne bewegliche Teile zur optischen Signalisierung von veränderlichen Informationen wie Bildern oder Zeichen.',150,'Bild, TV','',3,5,'assets/pictures/Monitor3.jpg'),
(4,1,'Hyper C Monitor','Ein Bildschirm (auch Monitor bzw. englisch Screen oder Display) ist eine elektrisch angesteuerte Anzeige ohne bewegliche Teile zur optischen Signalisierung von veränderlichen Informationen wie Bildern oder Zeichen.',222,'Bild, TV','',5, '','assets/pictures/Monitor4.jpg'),
(5,1,'Ultra Y Monitor','Ein Bildschirm (auch Monitor bzw. englisch Screen oder Display) ist eine elektrisch angesteuerte Anzeige ohne bewegliche Teile zur optischen Signalisierung von veränderlichen Informationen wie Bildern oder Zeichen.',250,'Bild, TV','',4,10,'assets/pictures/Monitor5.jpg'),
(6,1,'Ultra T Monitor','Ein Bildschirm (auch Monitor bzw. englisch Screen oder Display) ist eine elektrisch angesteuerte Anzeige ohne bewegliche Teile zur optischen Signalisierung von veränderlichen Informationen wie Bildern oder Zeichen.',239,'Bild, TV','',4, '','assets/pictures/Monitor6.jpg'),
(7,1,'Ultra Z Monitor','Ein Bildschirm (auch Monitor bzw. englisch Screen oder Display) ist eine elektrisch angesteuerte Anzeige ohne bewegliche Teile zur optischen Signalisierung von veränderlichen Informationen wie Bildern oder Zeichen.',299,'Bild, TV','',4,5,'assets/pictures/Monitor7.jpg'),
(8,1,'Ultra V Monitor','Ein Bildschirm (auch Monitor bzw. englisch Screen oder Display) ist eine elektrisch angesteuerte Anzeige ohne bewegliche Teile zur optischen Signalisierung von veränderlichen Informationen wie Bildern oder Zeichen.',100,'Bild, TV','',2, '','assets/pictures/Monitor8.jpg'),
(9,1,'Ultra M Monitor','Ein Bildschirm (auch Monitor bzw. englisch Screen oder Display) ist eine elektrisch angesteuerte Anzeige ohne bewegliche Teile zur optischen Signalisierung von veränderlichen Informationen wie Bildern oder Zeichen.',250,'Bild, TV','',3, '','assets/pictures/Monitor9.jpg'),
(10,1,'Hyper U Monitor','Ein Bildschirm (auch Monitor bzw. englisch Screen oder Display) ist eine elektrisch angesteuerte Anzeige ohne bewegliche Teile zur optischen Signalisierung von veränderlichen Informationen wie Bildern oder Zeichen.',559,'Bild, TV','',5, '','assets/pictures/Monitor10.jpg'),
(11,2,'Mec U Tastatur','Die Tastatur ist ein weiteres Eingabegerät und gehört zur Gruppe Peripherie.',19,'Eingabe','',2, '','assets/pictures/Tastatur1.jpg'),
(12,2,'Mec T Tastatur','Die Tastatur ist ein weiteres Eingabegerät und gehört zur Gruppe Peripherie.',30,'Eingabe, Gaming','',3, '','assets/pictures/Tastatur2.jpg'),
(13,2,'Mec C Tastatur','Die Tastatur ist ein weiteres Eingabegerät und gehört zur Gruppe Peripherie.',25,'Eingabe, Gaming','',3, '','assets/pictures/Tastatur3.jpg'),
(14,2,'Mec D Tastatur','Die Tastatur ist ein weiteres Eingabegerät und gehört zur Gruppe Peripherie.',15,'Eingabe','',4, '','assets/pictures/Tastatur4.jpg'),
(15,2,'Mec T Tastatur','Die Tastatur ist ein weiteres Eingabegerät und gehört zur Gruppe Peripherie.',45,'Eingabe, Gaming','',2, '','assets/pictures/Tastatur5.jpg'),
(16,2,'Mec P Tastatur','Die Tastatur ist ein weiteres Eingabegerät und gehört zur Gruppe Peripherie.',50,'Eingabe, Gaming','',3, '','assets/pictures/Tastatur6.jpg'),
(17,2,'Try C Tastatur','Die Tastatur ist ein weiteres Eingabegerät und gehört zur Gruppe Peripherie.',39,'Eingabe, Gaming','',3, '','assets/pictures/Tastatur7.jpg'),
(18,2,'Try F Tastatur','Die Tastatur ist ein weiteres Eingabegerät und gehört zur Gruppe Peripherie.',99,'Eingabe, Gaming','',3,15,'assets/pictures/Tastatur8.jpg'),
(19,2,'Try U Tastatur','Die Tastatur ist ein weiteres Eingabegerät und gehört zur Gruppe Peripherie.',70,'Eingabe, Gaming','',5, '','assets/pictures/Tastatur9.jpg'),
(20,2,'Try Q Tastatur','Die Tastatur ist ein weiteres Eingabegerät und gehört zur Gruppe Peripherie.',119,'Eingabe, Gaming','',4, '','assets/pictures/Tastatur10.jpg'),
(21,3,'Speedy T Maus','Die Computer-Maus dient zum Steuern des virtuellen Zeigers (Cursor), der auf dem Computer-Bildschirm dargestellt ist.',10,'Eingabe, Gaming','',1, '','assets/pictures/Maus1.jpg'),
(22,3,'Speedy D Maus','Die Computer-Maus dient zum Steuern des virtuellen Zeigers (Cursor), der auf dem Computer-Bildschirm dargestellt ist.',19,'Eingabe, Gaming','',1, '','assets/pictures/Maus2.jpg'),
(23,3,'Speedy F Maus','Die Computer-Maus dient zum Steuern des virtuellen Zeigers (Cursor), der auf dem Computer-Bildschirm dargestellt ist.',25,'Eingabe, Gaming','',2, '','assets/pictures/Maus3.jpg'),
(24,3,'Speedy Q Maus','Die Computer-Maus dient zum Steuern des virtuellen Zeigers (Cursor), der auf dem Computer-Bildschirm dargestellt ist.',33,'Eingabe, Gaming','',4, '','assets/pictures/Maus4.jpg'),
(25,3,'Speedy Z Maus','Die Computer-Maus dient zum Steuern des virtuellen Zeigers (Cursor), der auf dem Computer-Bildschirm dargestellt ist.',40,'Eingabe, Gaming','',3, '','assets/pictures/Maus5.jpg'),
(26,3,'Fast T Maus','Die Computer-Maus dient zum Steuern des virtuellen Zeigers (Cursor), der auf dem Computer-Bildschirm dargestellt ist.',15,'Eingabe, Gaming, Ergonomisch','',2, '','assets/pictures/Maus6.jpg'),
(27,3,'Fast F Maus','Die Computer-Maus dient zum Steuern des virtuellen Zeigers (Cursor), der auf dem Computer-Bildschirm dargestellt ist.',49,'Eingabe, Ergonomisch','',3,5,'assets/pictures/Maus7.jpg'),
(28,3,'Fast Z Maus','Die Computer-Maus dient zum Steuern des virtuellen Zeigers (Cursor), der auf dem Computer-Bildschirm dargestellt ist.',50,'Eingabe, Ergonomisch','',4, '','assets/pictures/Maus8.jpg'),
(29,3,'Fast U Maus','Die Computer-Maus dient zum Steuern des virtuellen Zeigers (Cursor), der auf dem Computer-Bildschirm dargestellt ist.',79,'Eingabe, Gaming, Ergonomisch','',5,5,'assets/pictures/Maus9.jpg'),
(30,3,'Fast G Maus','Die Computer-Maus dient zum Steuern des virtuellen Zeigers (Cursor), der auf dem Computer-Bildschirm dargestellt ist.',99,'Eingabe, Gaming, Ergonomisch','',4, '','assets/pictures/Maus10.jpg'),
(31,4,'In-Ear Ultra','Kopfhörer sind kleine Schallwandler, die an oder in den Ohren getragen werden.',199,'Soround, Bass','',5,10,'assets/pictures/InEar1.jpg'),(32,4,'In-Ear Alpha','Kopfhörer sind kleine Schallwandler, die an oder in den Ohren getragen werden.',99,'Soround, Bass','',3, '','assets/pictures/InEar2.jpg'),
(33,4,'In-Ear Beta','Kopfhörer sind kleine Schallwandler, die an oder in den Ohren getragen werden.',150,'Soround, Bass','',5,5,'assets/pictures/InEar3.jpg'),
(34,4,'In-Ear Gamma','Kopfhörer sind kleine Schallwandler, die an oder in den Ohren getragen werden.',170,'Soround, Bass','',5, '','assets/pictures/InEar4.jpg'),
(35,4,'Loud 2222 Headphone','Kopfhörer sind kleine Schallwandler, die an oder in den Ohren getragen werden.',189,'Laut, Bass','',2, '','assets/pictures/Kopfhoerer1.jpg'),
(36,4,'Compact','Kopfhörer sind kleine Schallwandler, die an oder in den Ohren getragen werden.',59,'Sound','',3, '', 'assets/pictures/Kopfhoerer2.jpg'),
(37,4,'Sorden','Kopfhörer sind kleine Schallwandler, die an oder in den Ohren getragen werden.',69,'Sound','',3, '', 'assets/pictures/Kopfhoerer3.jpg'),
(38,4,'GS 112','Kopfhörer sind kleine Schallwandler, die an oder in den Ohren getragen werden.',99,'Sound','',5, '', 'assets/pictures/Kopfhoerer4.jpg'),
(39,4,'Compact 2','Kopfhörer sind kleine Schallwandler, die an oder in den Ohren getragen werden.',110,'Sound','',5, '', 'assets/pictures/Kopfhoerer5.jpg'),
(40,4,'Razor','Kopfhörer sind kleine Schallwandler, die an oder in den Ohren getragen werden.',300,'Sound','',2, '','assets/pictures/Kopfhoerer6.jpg');


Connection:
---------
1. Verbindungsaufbau mit der Datenbank wird in der Datei connection.php erzeugt.

2. Felder mit *, durch eigene DB-Daten ersetzten.

$dbServername = "localhost";
$dbUser = "*";
$dbPassword = "*";
$dbName = "*";

if(!$conn = mysqli_connect($dbServername, $dbUser, $dbPassword, $dbName)){
    die("failed to connect");
}
