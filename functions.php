<?php

function email_check($conn,$mail){

    $z = 1;
    $sql = "select * from Benutzer where Mail = '$mail'";

    $result = mysqli_query($conn,$sql);

    if($result && mysqli_num_rows($result) > 0){
        $z = 0;
        return $z;
    }

    return $z;
}


function getItems($conn){

    #SELECT * FROM `Produkttabelle` GROUP BY ID_Kategorie
    $sql = "Select * from Produkttabelle";

    $result = $conn->query($sql);
    $array = array();

    while($items = mysqli_fetch_assoc($result)){
        $array[] = $items;
    }

    return $array;

}

function getItemsKat($conn,$kat){

    
    $sql = "Select * from Produkttabelle where ID_Kategorie = $kat";

    $result = $conn->query($sql);
    $array = array();

    while($items = mysqli_fetch_assoc($result)){
        $array[] = $items;
    }

    return $array;

}

function getItem($conn, $id_Produkt){

    $sql = "Select * from Produkttabelle where id_Produkt = $id_Produkt";

    $result = $conn->query($sql);
    $item = mysqli_fetch_assoc($result);

    return $item;

}

function getKategorieItems($conn, $id_Kategorie){

    $sql = "Select * from Produkttabelle where id_Kategorie = $id_Kategorie";
    
    $result = $conn->query($sql);
    $item = mysqli_fetch_assoc($result);
    
    return $item;
    
}

function getKategorieItemsSale($conn, $id_Kategorie){

    $sql = "Select * from Produkttabelle where id_Kategorie = $id_Kategorie AND Rabatt != 0";
    
    $result = $conn->query($sql);
    $item = mysqli_fetch_assoc($result);
    
    return $item;
    
}

function shop_items($items_array){

    $i = 0;
    
    echo "<div class='container px-4 px-lg-5 mt-5'>";
        echo "<div class='row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center'>"; 

        while($i < count($items_array)){

            $name = $items_array[$i]['Name'];
            $bild = $items_array[$i]['Bild'];
            $Preis = $items_array[$i]['Preis'];
            $Katergorie = $items_array[$i]['ID_Kategorie'];
            $Rabatt = $items_array[$i]['Rabatt'];

            echo "<div class='col mb-5'>"; 
                echo "<div class='card h-100'>";
                #---Image
                if (empty($items_array[$i]['Bild'])){
                    echo "<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt='...' />";    
                }else{
                    echo "<img class='card-img-top' src='$bild' alt='...' />";
                }
                    #---product details
                    echo "<div class='card-body p-4'";
                        echo "<div class='text-center'>";
                    #---Product name
                            echo "<h5 class='fw-bolder'>$name</h5>";
                            #---Product price
                            echo "$Preis €";
                        echo "</div>";
                    echo "</div>";
                    #---Product actions
                    echo "<div class='card-footer p-4 pt-0 border-top-0 bg-transparent'>";
                        echo "<div class='text-center'><a class='btn btn-outline-dark mt-auto' href='#'>View options</a></div>";
                    echo "</div>";
                echo "</div>";
            echo "</div>";

            $i= $i +1;
        }

        echo "</div>";
    echo "</div>";
}

function check_login($conn){
    # Tutorial: https://www.youtube.com/watch?v=WYufSGgaCZ8

    if(isset($_SESSION['mail'])){
        
        
        $mail = $_SESSION['mail'];

        $sql = "select * from Benutzer where Mail = '$mail'";

        $result = mysqli_query($conn,$sql);
    
        if($result && mysqli_num_rows($result) > 0){
            $user = mysqli_fetch_assoc($result);

            echo "<div class='d-flex ml-auto'>";
                echo "<a class='btn btn-outline-dark me-1' href='profile.php' type='button'>";
                    echo "<i class='bi-person me-1'></i>";
                    echo "Profil";
                echo "</a>";
                
                echo "<a class='btn btn-outline-dark me-1' href='logout.php' type='button'>Logout</a>";
                echo "<a class='btn btn-outline-dark' href='shopping-cart.php' type='button'>";
                echo "<i class='bi-cart-fill me-1'></i>";
                echo "Cart";

                $anzahl = anzahl_wk($conn,$user['ID_Nutzer']);

                echo "<span class='badge bg-dark text-white ms-1 rounded-pill'>$anzahl</span>";
                echo "</a>";
            echo "</div>";
            return $user;
        }
    }
    
    echo "<div class='d-flex ml-auto'>";
    echo "<a class='btn btn-outline-dark me-1' href='login.php' type='button'>Login</a>";
    echo "</div>";
}

function anzahl_wk($conn,$ID_Nutzer){

    $sql = "select * from Warenkorb where ID_Nutzer = '$ID_Nutzer'";

    $result = mysqli_query($conn,$sql);
    
    if(($result && mysqli_num_rows($result))>0){
        $rows = mysqli_num_rows($result);
    }else{
        $rows = 0;
    }

    return $rows;

}


function warenkorb($conn){

    $preis_all = 0;
    $mail = $_SESSION['mail'];
    $sql = "select * from Benutzer where Mail = '$mail'";

    $result = mysqli_query($conn,$sql);

    if($result && mysqli_num_rows($result) > 0){
        $user = mysqli_fetch_assoc($result);

        $ID_Nutzer = $user['ID_Nutzer'];
        
        $sql = "select * from Warenkorb where ID_Nutzer = '$ID_Nutzer'";

            $result = mysqli_query($conn,$sql);
        if(($result && mysqli_num_rows($result))>0){
        
            while($items = mysqli_fetch_assoc($result)){

                $ID_Produkt = $items['ID_Produkt'];

                $item = getItem($conn,$ID_Produkt);

                $name = $item['Name'];
                $bild = $item['Bild'];
                $Anzahl = $items['Anzahl'];
                $Gesamt = $items['Gesamt'];

                $preis_all = $preis_all + $Gesamt;

                echo "<div class='card rounded-3 mb-4'>";
                    echo "<div class='card-body p-4'>";
                        echo "<div class='row d-flex justify-content-between align-items-center'>";
                            echo "<div class='col-md-2 col-lg-2 col-xl-2'>";
                                if (empty($bild)){
                                    echo "<img class='img-fluid rounded-3'src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg'  alt='Cotton T-shirt'>";    
                                }else{
                                    echo "<img class='img-fluid rounded-3' src='$bild' alt='Cotton T-shirt'>";
                                }
                            echo "</div>";
                            echo "<div class='col-md-3 col-lg-3 col-xl-3'>";
                                echo "<p class='lead fw-normal mb-2'>$name</p>";
                            echo "</div>";
                            echo "<div class='col-md-3 col-lg-3 col-xl-2 d-flex'>";
                                echo "<p class='lead fw-normal mb-2'>Anzahl: $Anzahl</p>";
                            echo "</div>";
                            echo "<div class='col-md-3 col-lg-2 col-xl-2 offset-lg-1'>"; 
                                echo "<h5 class='mb-0'>$Gesamt €</h5>"; 
                            echo "</div>"; 
                            echo "<div class='col-md-1 col-lg-1 col-xl-1 text-end'>"; 
                                echo "<span><a href='shopping-cart.php?pid=$ID_Produkt' class='text-danger'><i class='bi bi-trash-fill'></i></span></a>"; 
                            echo "</div>"; 
                        echo "</div>"; 
                    echo "</div>"; 
                echo "</div>";
             
            }                           
        }   
    }
    return $preis_all; 
}

function profile($conn){

    if(isset($_SESSION['mail'])){
        
        $mail = $_SESSION['mail'];

        $sql = "select * from Benutzer where Mail = '$mail'";

        $result = mysqli_query($conn,$sql);
    
        if($result && mysqli_num_rows($result) > 0){
            $user = mysqli_fetch_assoc($result);
            $vorname = $user['Vorname'];
            $nachname = $user['Nachname'];
            $strasse = $user['Strasse'];
            $ort = $user['Ort'];
            $mail = $user['Mail'];
            $zahlmittel = $user['Zahlungsdaten'];

            echo "<form method ='post'>";
            echo "<div class='row mt-2'>";
                echo "<div class='col-md-6'><label class='labels'>Vorname</label><input type='text' class='form-control' placeholder='Vorname' name='vorname' value='$vorname'></div>";
                echo "<div class='col-md-6'><label class='labels'>Nachname</label><input type='text' class='form-control' placeholder='Nachname' name='nachname' value='$nachname'></div>";
            echo "</div>";
            echo "<div class='row mt-3'>";
                echo "<div class='col-md-12'><label class='labels'>Straße</label><input type='text' class='form-control' placeholder='Straßennamen eingeben' name ='str' value='$strasse'></div>";
                echo "<div class='col-md-12'><label class='labels'>Ort</label><input type='text' class='form-control' placeholder='Ort eingeben' name='ort' value='$ort'></div>";
                echo "<div class='col-md-12'><label class='labels'>E-Mail</label><input type='text' class='form-control' placeholder='E-Mail eingeben' name='email' value='$mail'></div>";
                echo "<div class='col-md-12'><label class='labels'>Passwort ändern</label><input type='text' class='form-control' placeholder='Passwort eingeben' name='pw' value=''></div>";
                echo "<div class='col-md-12'><label class='labels'>Zahlungsdaten</label><input type='text' class='form-control' placeholder='Zahlungsdaten eingeben' name='zahlung' value='$zahlmittel'></div>";
            echo "</div>";
            echo "<div class='mt-5 text-center'>";
            echo "<button type='submit' name='button1' class='btn btn-primary profile-button' >Save Profile</button>";
            echo "</div>";
            echo "</form>";

            return $user;
        }
    }
    else{
        unset($_SESSION['mail']);
        header("location: login.php");
    }

}


function shop($conn,$kat){

        $sql = "Select * from Produkttabelle where ID_Kategorie = $kat";
        $result = mysqli_query($conn,$sql);

    echo "<div class='container px-4 px-lg-5 mt-5'>";
        echo "<div class='row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center'>"; 

            if($result && mysqli_num_rows($result) > 0){   
                while($item = mysqli_fetch_assoc($result)){

                    $name = $item['Name'];
                    $id = $item['ID_Produkt'];
                    $bild = $item['Bild'];
                    $preis = $item['Preis'];
                    $rabatt = $item['Rabatt'];
                    $preis_rabatt = (100-$rabatt)/100 * $preis;
                    $stars = $item['Bewertung'];
                    $y = 0;
                    echo "<div class='col mb-5'>";
                        echo "<div class='card h-100'>";
                            #<!-- Product image-->
                            if (empty($bild)){
                                echo "<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt='...' />";    
                            }else{
                                echo "<img class='card-img-top' src='$bild' alt='...' />";
                            }
                            #<!-- Product details-->
                            echo "<div class='card-body p-4'>";
                                echo "<div class='text-center'>";
                                    #<!-- Product name-->
                                    echo "<h5 class='fw-bolder'>$name</h5>";
                                    echo "<div class='d-flex justify-content-center small text-warning mb-2'>";
                                    while ($y<$stars){
                                        echo "<div class='bi-star-fill'></div>";
                                        $y = $y + 1;
                                    }
                                    echo "</div>";
                                    #<!-- Product price-->
                                    if (is_null($rabatt) || $rabatt == 0){
                                        echo "$preis €";
                                    }else{
                                        echo "<span class='text-muted text-decoration-line-through'>$preis €</span>";
                                        echo "  $preis_rabatt €"; 
                                    } 
                                echo "</div>";
                                #<!-- Product actions-->
                                echo "<div class='card-footer p-4 pt-0 border-top-0 bg-transparent'>";
                                    echo "<div class='text-center'><span><a class='btn btn-outline-dark mt-auto' href='product.php?pid=$id'>View options</a></span></div>";
                                echo "</div>";
                            echo "</div>";
                        echo "</div>";
                    echo "</div>";                          
                }
                echo "</div>";
            echo "</div>";  
            }
}

function shop_all($conn){

    $sql = "Select * from Produkttabelle";
    $result = mysqli_query($conn,$sql);

echo "<div class='container px-4 px-lg-5 mt-5'>";
    echo "<div class='row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center'>"; 

        if($result && mysqli_num_rows($result) > 0){   
            while($item = mysqli_fetch_assoc($result)){

                $name = $item['Name'];
                $bild = $item['Bild'];
                $id = $item['ID_Produkt'];
                $preis = $item['Preis'];
                $rabatt = $item['Rabatt'];
                $preis_rabatt = (100-$rabatt)/100 * $preis;
                $stars = $item['Bewertung'];
                $y = 0;
                
                echo "<div class='col mb-5'>";
                    echo "<div class='card h-100'>";
                        #<!-- Product image-->
                        if (empty($bild)){
                            echo "<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt='...' />";    
                        }else{
                            echo "<img class='card-img-top' src='$bild' alt='...' />";
                        }
                        #<!-- Product details-->
                        echo "<div class='card-body p-4'>";
                            echo "<div class='text-center'>";
                                #<!-- Product name-->
                                echo "<h5 class='fw-bolder'>$name</h5>";
                                echo "<div class='d-flex justify-content-center small text-warning mb-2'>";
                                    while ($y<$stars){
                                        echo "<div class='bi-star-fill'></div>";
                                        $y = $y + 1;
                                    }
                                echo "</div>";
                                #<!-- Product price-->
                                if (is_null($rabatt) || $rabatt == 0){
                                    echo "$preis €";
                                }else{
                                    echo "<span class='text-muted text-decoration-line-through'>$preis €</span>";
                                    echo "  $preis_rabatt €"; 
                                } 
                            echo "</div>";
                            #<!-- Product actions-->
                            echo "<div class='card-footer p-4 pt-0 border-top-0 bg-transparent'>";
                                echo "<div class='text-center'><span><a class='btn btn-outline-dark mt-auto' href='product.php?pid=$id'>View options</a></span></div>";
                            echo "</div>";
                        echo "</div>";
                    echo "</div>";
                echo "</div>";                          
            }
            echo "</div>";
        echo "</div>";  
        }
}

function product_page($conn,$id){

    $sql = "Select * from Produkttabelle where id_Produkt = $id";

    $result = $conn->query($sql);
    $item = mysqli_fetch_assoc($result);

    $name = $item['Name'];
    $beschreibung = $item['Beschreibung'];
    $preis = $item['Preis'];
    $tags = $item['Tags'];
    $infos = $item['Infos'];
    $bewertung = $item['Bewertung'];
    $rabatt = $item['Rabatt'];
    $bild = $item['Bild'];

    
    echo "<form method ='post'>";
    echo "<div class='container'>";
        echo "<div class='row mt-x'>";
                echo "<div class='col-md-5'>";
                if (empty($bild)){
                    echo "<img src='<img class='card-img-top' src='https://dummyimage.com/450x300/dee2e6/6c757d.jpg' alt=''...' class='img-fluid#>";
                }else{
                    echo "<img src='$bild' class='img-fluid'>";
                }
                echo "</div>";
                echo "<div class='col-md-7'>";
                    echo "<h2>$name</h2>";
                    echo "<p>Beschreibung: $beschreibung <br /><br />
                        Infos: $infos <br /><br />    
                        Bewertung: $bewertung Sterne <br /><br />";
                    if(empty($rabatt)){
                        echo "Preis: $preis €<br /><br />";
                    }else{
                        $rabatt_preis = (100-$rabatt)/100 * $preis;
                        echo "Alter Preis: $preis €<br />
                        Neuer Preis: $rabatt_preis €<br /><br />";
                    }
                    echo "Tags: $tags <br /><br />

                        </p>";
                echo "<label class='labels'>Anzahl:</label><input type='text' placeholder='Anzahl eingeben' name='anzahl' >";
                echo "<p><br />Maximal 5 p. Produkt<br /></p>";
                echo "<button type ='submit' name='ware' class='btn btn-secondary'>Zum Warenkorb hinzufügen</button>";
                echo "</div>";
           
        echo "</div>";
    echo "</div>";
    echo "</form>";

}

function getKunde($conn){
    
    $mail = $_SESSION['mail'];

    $sql = "Select * from Benutzer where Mail = '$mail'";

    $result = $conn->query($sql);
    $item = mysqli_fetch_assoc($result);

    return $item;

}


function ware_hinzufügen($conn,$id_prod,$anzahl){
    
    if (empty($anzahl) || ($anzahl == 0) || ($anzahl > 5 || is_int($anzahl))){
        echo "Anzahl nicht möglich!";
    }else{

        $benutzer = getKunde($conn);
        $id_nutzer = $benutzer['ID_Nutzer'];
        $item = getItem($conn,$id_prod);
        $preis = $item['Preis'];
        $rabatt = $item['Rabatt'];

        if (is_null($rabatt) || $rabatt == 0){

        }else{
            $preis = (100-$rabatt)/100 * $preis;
        }
        $sql = "Select * from Warenkorb where ID_Produkt = $id_prod AND ID_Nutzer = $id_nutzer";

        $result = mysqli_query($conn,$sql);
        
        if($result && mysqli_num_rows($result) > 0){
            $item = mysqli_fetch_assoc($result);
            
            $anzahl_ware = $item['Anzahl'];

            $anzahl_ges = $anzahl + $anzahl_ware;

            $gesamt_preis = $preis * $anzahl_ges;

            if ($anzahl_ges > 5){
                echo "Zu viele Produkte insgesamt";
                return $anzahl_ges;
            }else{
                $sql ="update Warenkorb 
                        set Anzahl = $anzahl_ges, Gesamt = $gesamt_preis 
                        where ID_Produkt= $id_prod AND ID_Nutzer = $id_nutzer";
                        echo "Ware hinzugefügt!";
                $conn->query($sql);
                return $anzahl_ges;  
            }
        }else{
            $gesamt_preis = $preis * $anzahl;
            $sql = "INSERT INTO Warenkorb (ID_Nutzer, ID_Produkt, Anzahl, Gesamt) VALUES ($id_nutzer,$id_prod,$anzahl,$gesamt_preis)";
            $conn->query($sql);
            return $anzahl;
        }
    }
}

function get_warenkorb($conn,$id_prod,$id_nutzer){

    $sql = "Select * from Warenkorb where ID_Nutzer = $id_nutzer AND ID_Produkt = $id_prod";

    $result = $conn->query($sql);
    $item = mysqli_fetch_assoc($result);

    return $item;
}

function delete_item($conn,$id_prod){
    
    $benutzer = getKunde($conn);
    $id_nutzer = $benutzer['ID_Nutzer'];
    $item = getItem($conn,$id_prod,$id_nutzer);
    $warenkorb = get_warenkorb($conn,$id_prod,$id_nutzer);
    $anzahl = $warenkorb['Anzahl'];
    $preis = $item['Preis'];
    $rabatt = $item['Rabatt'];

    if ($anzahl == 1){
        $sql = "delete from Warenkorb WHERE ID_Nutzer = $id_nutzer AND ID_Produkt = $id_prod";
        $conn->query($sql);
        header("location: shopping-cart.php");   
    }else{
        
        $anzahl = $anzahl - 1;
        
        if (empty($rabatt)){

        }else{
            $preis = (100-$rabatt)/100 * $preis;
        }
        $gesamt_preis = $anzahl * $preis;

        $sql ="update Warenkorb 
                set Anzahl = $anzahl, Gesamt = $gesamt_preis 
                where ID_Produkt= $id_prod AND ID_Nutzer = $id_nutzer";
        $conn->query($sql);
        header("location: shopping-cart.php");

    }

}

function get_warenkorb_all($conn,$id_nutzer){

    $sql = "Select * from Warenkorb where ID_Nutzer = $id_nutzer";

    $result = $conn->query($sql);
    $array = array();
    
    while($items = mysqli_fetch_assoc($result)){
        $array[] = $items;
    }

    return $array;
}

function count_bestellungen($conn,$id_nutzer){
    
    $sql = "SELECT * FROM Bestellungen WHERE ID_Nutzer = $id_nutzer GROUP BY ID_Bestellung";

    $result = $conn->query($sql);
    $array = array();
    $i = 0;
    
    while($items = mysqli_fetch_assoc($result)){
        $i = $i + 1;
    }

    return $i;

}

function bezahlen($conn){
    
    $benutzer = getKunde($conn);
    $id_nutzer = $benutzer['ID_Nutzer'];
    $zahlung = $benutzer['Zahlungsdaten'];

    if(!empty($zahlung)){
        $ware = get_warenkorb_all($conn,$id_nutzer);
        $i = 0;
        $id_bestellung = count_bestellungen($conn,$id_nutzer) + 1;
        $summe = 0;
        while ($i < count($ware)){
   
            $anzahl = $ware[$i]['Anzahl'];
            $gesamt = $ware[$i]['Gesamt'];
            $id_prod = $ware[$i]['ID_Produkt'];
            $summe = $summe + $gesamt;
            $test = count($ware);
            echo $test;

            $sql = "INSERT INTO Bestellungen (ID_Bestellung, ID_Nutzer, ID_Produkt, Anzahl, Gesamt) VALUES ($id_bestellung, $id_nutzer,$id_prod,$anzahl,$gesamt)";
            $i = $i + 1;
            $conn->query($sql);
            
            $sql = "DELETE FROM Warenkorb WHERE ID_Nutzer = $id_nutzer";
            $conn->query($sql);
        }

        $sql = "INSERT INTO Rechnung (ID_Nutzer, Gesamt) VALUES ($id_nutzer,$summe)";
        $conn->query($sql);

    }else{
        echo '<script>', 'myfunction();', '</script>';
    }

}

function bestellungen($conn){
    $benutzer = getKunde($conn);
    $id_nutzer = $benutzer['ID_Nutzer'];

    $count = count_bestellungen($conn,$id_nutzer);

    $y = 1;

    while($y < $count + 1){

        $sql = "SELECT * FROM Bestellungen WHERE ID_Nutzer = $id_nutzer AND ID_Bestellung = $y";

        $result = $conn->query($sql);
        $array = array();

        echo "<h4>Bestellung: $y</h4>";
        echo "<p>";
        $summe = 0;

        while($bestellung = mysqli_fetch_assoc($result)){
            $id_prod = $bestellung['ID_Produkt'];
            $item = getItem($conn,$id_prod);
            $name = $item['Name'];
            $preis = $bestellung['Gesamt'];
            $anzahl = $bestellung['Anzahl'];

            $summe = $summe + $preis;
            echo "Bezeichnung: $name <br />";
            echo "Anzahl: $anzahl <br />"; 
            echo "Preis: $preis <br />";      
        }

        echo "Summe: $summe <br />";
        echo "</p>";

        $y = $y + 1;
    }
}